import config from '../../config/env'
import axios from 'axios'

const resource = axios.create({
    baseURL: config.baseUrl
});

export default resource;
