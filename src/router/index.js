import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '@/components/Homepage'
import Register from '@/components/Auth/Register'
import Login from '@/components/Auth/Login'

import PostIndex from '@/components/Post/Index'
import PostCreate from '@/components/Post/Create'
import PostUpdate from '@/components/Post/Update'
import PostView from '@/components/Post/View'
import ChangePassword from '@/components/Auth/ChangePassword'

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'homepage',
            component: Homepage
        },
        //Auth
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        //Post
        {
            path: '/post',
            name: 'post.index',
            component: PostIndex
        },
        {
            path: '/post/create',
            name: 'post.create',
            component: PostCreate
        },
        {
            path: '/post/:id/update',
            name: 'post.update',
            component: PostUpdate
        },
        {
            path: '/post/:id',
            name: 'post.view',
            component: PostView
        },
        {
            path: '/changepassword',
            name: 'change.password',
            component : ChangePassword
        }
    ]
})
