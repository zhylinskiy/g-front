import http from '../http/index'

export default {
    user: {
        authenticated: false,
        profile: null
    },
    register(context, email, name, password, passwordConfirm) {
        http.post('auth/register', {
            email: email,
            name: name,
            password: password,
            password_confirmation: passwordConfirm
        }).then(response => {
            context.success = response.success;
            context.$router.push('login');
        }, error => {
            context.errors = error.response.data.errors;
            context.message = error.response.data.message;
        })
    },
    login(context, email, password) {
        http.post('auth/login', {
            email: email,
            password: password
        }).then((response) => {
            context.success = response.data.success;

            localStorage.setItem('id_token', response.data.data.token);
            this.setAuthHeader();

            this.user.authenticated = true;

            context.$router.push({name: 'homepage'});
        }, error => {
            context.errors = error.response.data.errors;
            console.log(error.response.data.message, error.response.data.errors);
        })
    },
    logout(context) {
        http.get('/auth/logout').then(response => {
            if (typeof response !== 'undefined') {
                localStorage.removeItem('id_token');
            }
            context.$router.push('login');
        })
    },
    setAuthHeader() {
        let token = localStorage.getItem('id_token');

        if (token !== null) {
            http.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token');
        }
    },
    checkToken(context) {
        this.setAuthHeader();

        http.get('auth/check').then(response => {
            if (typeof response !== 'undefined') {
                this.user.authenticated = response.data.success;
            }

            context.loaded = true;
        }, error => {
            context.loaded = true;
            context.$router.push({name: 'login'})
        });
    },
    changePassword(context) {
        http.post('change-password', {
            password: context.password,
            newPassword: context.newPassword
        }).then(response => {
            if (typeof response !== 'undefined') {
                localStorage.removeItem('id_token');

                //refresh page
                window.location.href = window.location.href;
            }
            context.errors = error.response.data.errors;
        })
    }
}