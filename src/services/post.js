import http from '../http/index'

export default {
    index(context) {
        http.get('/post').then(response => {
            context.loaded = response.data.success;
            context.posts = response.data.data;
        })
    },
    get(context, id) {
        http.get('/post/' + id).then(response => {
            context.loaded = response.data.success;
            context.post = response.data.data;
        })
    },
    create(context, title, body) {
        http.post('/post', {
            title: title,
            body: body
        }).then(response => {
            context.success = response.success;
            context.$router.push({name: 'homepage'});
        }, error => {
            context.errors = error.response.data.errors;
            context.message = error.response.data.message;
        })
    },
    update(context) {
        http.put('/post/' + context.post.id, {
            title: context.post.title,
            body: context.post.body,
        }).then(response => {
            context.loaded = response.data.success;
            context.$router.push({name: 'showposts'});
        })
    },
    destroy(context, id) {
        http.delete('/post/' + id).then(response => {
            context.loaded = response.data.success;
        })
    }
}
